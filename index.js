"use strict";
const S3FileSystem = require("./lib/S3FileSystem");
const sharp = require("sharp");

// Lambda Handler
exports.handler = (event, context, callback) => {
	console.log('Started');

	const data = JSON.parse(decodeURIComponent(event.body).replace('data=',''));
	const bucket = 'images.luminhouse.com';
	const key = data.userId + '/' + data.eventId + '/' + data.galleryId + '/' + data.fileName;
	console.log(key);
	const sizes = data.sizes.sort((a, b) => b.width - a.width);
	const results = [];

	const fileSystem = new S3FileSystem();

	fileSystem.getObject(bucket, key)
		.then(originalFile => rotate(originalFile, sizes[0]));

	const rotate = (originalFile, size) => {
        console.log('Runing EXIF rotation');
		sharp(originalFile._data)
			.rotate()
			.toBuffer()
			.then(function(buffer){
				originalFile._data = buffer;
				resize(originalFile, sizes[0]);
			}).catch(function(err) {
				console.log(err);
			});
	}

	const resize = (imageData, size) => {
        console.log('Resizing ' + size.name + '(' + size.width + ')');
		sharp(imageData._data)
			.resize(size.width, size.height)
			.max()
			.jpeg()
			.toBuffer()
			.then(function(newData) {
				const newFileName =  Math.random().toString(36).slice(2) + '.' + imageData._type.ext;
				const newKey = data.userId + '/' + data.eventId + '/' + data.galleryId + '/resized/' + newFileName;

				const imageType = {
					ext: 'jpg',
					mime: 'image/jpeg'
				};

				const newFile = {
					_fileName: newKey,
					_bucketName: 'images.luminhouse.com',
					_data: newData,
					_headers: { ContentType: 'image/jpeg', CacheControl: undefined },
					_acl: undefined,
					_type: imageType
				};

				upload(newFile, size);

				let index = sizes.indexOf(size);
				index++;

				if(index < sizes.length){
					resize(newFile, sizes[index]);
				}

			}).catch(function(err) {
				console.log(err);
			});
	};

	const upload = (newFile, size) =>{
		size.url = 'https://s3-eu-west-1.amazonaws.com/images.luminhouse.com/' + newFile._fileName;
		fileSystem.putObject(newFile)
			.then(function (data) {
				processCallback(size);
			}).catch(function (err) {
			  console.log(err);
			});
	};

	const processCallback = newResult => {
		results.push(newResult);

		if(results.length === sizes.length){
			const message = "OK, " + results.length + " images were processed.";
			const response = {
				"isBase64Encoded": false,
				"statusCode": 200,
				"headers": {
					"Access-Control-Allow-Origin" : "*"
				},
				"body": JSON.stringify({
					results: results,
					message: message
				})
			};
			console.log("response: " + JSON.stringify(response));
			callback(null, response);
			return;
		}
	}
};
